import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import store from "./store";
import App from './App';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import User from './components/User';
import Navigation from './components/Navigation';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Navigation />
      <Switch>
        <Route path="/user">
          <User />
        </Route>
        <Route path="/">
          <App />
        </Route>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);

