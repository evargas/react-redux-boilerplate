import { combineReducers } from "redux";
import selection from "./players";
import usersFetch from "./users";

export default combineReducers({
  selection,
  usersFetch
});
