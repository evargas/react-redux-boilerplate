import React from 'react'
import { Link } from "react-router-dom"
import './Navigation.scss'

export default () => (
  <nav className="main-navigation">
    <ul>
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/user">Users</Link>
      </li>
    </ul>
  </nav>
);